import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireAction, DatabaseSnapshot, AngularFireList } from 'angularfire2/database';
import * as firebase from "firebase/app";
import { HttpHeaders, HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DbServiceService {

  //variable que guardará provisionalmente los vídeos vistos por el usuario en su sesión
  userVideos: any;

  //ruta en el storage
  private basePath: string = '/uploads';

  constructor(private ag: AngularFireDatabase,
    private http: HttpClient) { }

  //trae los vídeos del nodo videos
  public getVideos(url: string) {
    return firebase.database().ref(url).once('value');
  }

  //inserta un video en el nodo de videos
  public async addvideos(videodata:any){
    var url = 'videos';
    console.log("datavideo en el service: ", videodata);
    return await firebase.database().ref(url).push(videodata).key;
  }

  //inserta un video en el nodo de videos
  public async addvideosForgotten(videodata:any){
    var url = 'forgotten';
    console.log("datavideo en el service: ", videodata);
    return await firebase.database().ref(url).push(videodata).key;
  }

  //remover videos olvidados del nodo de videos
  public async removeNode(url: string) {
    return await firebase.database().ref(url).remove();
  }

  //añadir comentario a un vídeo
  addComent(body:any, idVideo:any){
    console.log("el comentario que llego al servicio: ", body);

    return firebase.database().ref("videos/"+idVideo+"/"+"comments").ref.push(body);
  }

  //traer comentarios
  public async getComments(videoKey: any, path: any) {
    return await firebase.database().ref(`${path}/${videoKey}/comments`).once("value");
  }

  //para actualizar la fecha fin de un video
  public async updateNode(url: any, data: any) {
    return await firebase.database().ref(url).update(data);
  }


  //verificar api twitter
  makeCall(){
    var headers = new HttpHeaders();
    headers.set('Content-Type', 'application/X-www-form-urlencoded');
    return this.http.post('http://localhost:3000/authorize', { headers: headers });
  }


  //pedir tweets
  searchcall(searchQuery:any) {
    var headers = new HttpHeaders();
    var searchterm = 'query=' + searchQuery;

    headers.set('Content-Type', 'application/X-www-form-urlencoded');

    return this.http.post('http://localhost:3000/search', searchterm, { headers: headers });
  }



  //guardar provisionalmente los vídeos vistos
  saveProvisionally(videos:any){
    console.log("la data que me llego de videos vistos: ", videos);
    this.userVideos= videos;

    // sessionStorage.setItem("videos", this.userVideos);

  }
  //manda los vídeos que la persona ha visto en su sesión
  getProvisionallyVideos(){
    return this.userVideos;
  }

  //inserta la historia de la persona en la bd
  public async addHisotry(videodata: any) {
    var url = 'histories';
    console.log("datavideo en el service: ", videodata);
    return await firebase.database().ref(url).push(videodata).key;

  }
  //trae las historias del nodo histories
  public getHistories(url: string) {
    return firebase.database().ref(url).once('value');
  }


}
