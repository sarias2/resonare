import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { InicioComponent } from './components/inicio/inicio.component';
import { MemorialesComponent } from './components/memoriales/memoriales.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PanelAdministradorComponent } from './panel-administrador/panel-administrador.component';

const routes: Routes = [
    { path: '', component: InicioComponent },
    { path: 'inicio', component: InicioComponent },
    { path: 'memorias', component: MemorialesComponent },
    { path: 'nosotros', component: NosotrosComponent },
    { path: 'admin', component: PanelAdministradorComponent },
    { path: '**', component: NotFoundComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class FeatureRoutingModule {}
