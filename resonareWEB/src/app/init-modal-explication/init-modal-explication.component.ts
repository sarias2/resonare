import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-init-modal-explication',
  templateUrl: './init-modal-explication.component.html',
  styleUrls: ['./init-modal-explication.component.scss']
})
export class InitModalExplicationComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }


  closeModal(){
    this.activeModal.close();
  }

}
