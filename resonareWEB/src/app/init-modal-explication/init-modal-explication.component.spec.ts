import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitModalExplicationComponent } from './init-modal-explication.component';

describe('InitModalExplicationComponent', () => {
  let component: InitModalExplicationComponent;
  let fixture: ComponentFixture<InitModalExplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitModalExplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitModalExplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
