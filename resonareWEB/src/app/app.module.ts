import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { MemorialesComponent } from './components/memoriales/memoriales.component';
import { environment } from '../environments/environment';

//angular
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

//services  
import { DbServiceService } from './_services/db-service.service';
import { NotFoundComponent } from './components/not-found/not-found.component';


//bootstrap
import { NgbModule, NgbModal, NgbModalModule, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';



//material
import { MatButtonModule, MatCheckboxModule, MatDialog, MatDialogModule, MatFormFieldModule, MatExpansionModule } from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';





//routes
import { FeatureRoutingModule } from "./app.routes";
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ModalVideoComponent } from './components/shared/modal-video/modal-video.component';
import { InitVideoComponent } from './components/shared/init-video/init-video.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommentContentComponent } from './components/comment-content/comment-content.component';
import { HttpClientModule } from '@angular/common/http';
import { CreateVideoComponent } from './components/create-video/create-video.component';
import { ModalExplicationComponent } from './components/modal-explication/modal-explication.component';
import { LoadingVideoComponent } from './components/loading-video/loading-video.component';
import { CreateHistoryComponent } from './create-history/create-history.component';
import { YoutubePipePipe } from './youtube-pipe.pipe';
import { FormatDatePipe } from './format-date.pipe';
import { InitModalExplicationComponent } from './init-modal-explication/init-modal-explication.component';
import { PanelAdministradorComponent } from './panel-administrador/panel-administrador.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    NosotrosComponent,
    MemorialesComponent,
    NotFoundComponent,
    NavbarComponent,
    ModalVideoComponent,
    InitVideoComponent,
    CommentContentComponent,
    CreateVideoComponent,
    ModalExplicationComponent,
    LoadingVideoComponent,
    CreateHistoryComponent,
    YoutubePipePipe,
    FormatDatePipe,
    InitModalExplicationComponent,
    PanelAdministradorComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features,
    AngularFireDatabaseModule,
    FeatureRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatMenuModule,
    MatToolbarModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    NgbModule,
    NgbModalModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    MatExpansionModule
  ],
  entryComponents: [
    ModalVideoComponent,
    InitVideoComponent,
    CreateVideoComponent,
    ModalExplicationComponent,
    LoadingVideoComponent,
    CreateHistoryComponent,
    MemorialesComponent,
    InitModalExplicationComponent
  ],
  providers: [DbServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
