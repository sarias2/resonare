import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {
  today= new Date();
  transform(endDate:string):string {
    // console.log("la fecha en string: ", endDate);
    // console.log("la fecha en date: ", new Date(endDate));
    return formatDate(endDate, 'dd-MM-yyyy / hh:mm:ss - a', 'en-US', '+PDT');
  }

}
