import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DbServiceService } from '../_services/db-service.service';
import { LoadingVideoComponent } from '../components/loading-video/loading-video.component';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-create-history',
  templateUrl: './create-history.component.html',
  styleUrls: ['./create-history.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateHistoryComponent implements OnInit {

  //contenedor de los vídeos:
  videoContent=[];
  titleVideo="Título Historia";


  //fecha actual
  today = new Date();
  dateToday = '';

  constructor(public activeModal: NgbActiveModal,
    private dbs: DbServiceService,
    private modalService: NgbModal) { 

    //fecha actual
    this.dateToday = formatDate(this.today, 'dd-MM-yyyy / hh:mm:ss - a', 'en-US', '+PDT');
  }

  ngOnInit() {
    //traer los vídeos vistos por el usuario
    this.videoContent = this.dbs.getProvisionallyVideos();
    console.log("los videos vistos por el usuario: ", this.videoContent);
  }

  ngAfterViewInit(){
    
  }

  //insertar historia en la bd
  async publishHistory(){

    //se asigna la fecha de fin en formato date
    var endDateContent = this.sumarDias(this.today, +5);

    //pasando la fecha de creación  string
    let endDateString = endDateContent.toDateString();

    let video={
      date: this.dateToday,
      endDate: endDateString,
      videos: this.videoContent,
      title: this.titleVideo,
      views: 0
    }
    console.log("la historia: ", video);

    //insertae historia
    const result = await this.dbs.addHisotry(video);
    console.log("result: ", result);

    // abrir modal de retroalimentación
    const modalRefLogin = this.modalService.open(LoadingVideoComponent, { size: 'lg', windowClass: 'loadingVideoModal' });
    modalRefLogin.componentInstance.p1 ="Tu historia se esta insertando en nuestra base de datos";
    modalRefLogin.componentInstance.type ="historia";
    this.activeModal.close(); 
  }

  //remover un video de la historia
  removeVideo(keyVideo){
    console.log("keyVideo removida: ", keyVideo);
    //busca el servicio que quiere eliminar y lo elimina
    for (let i = this.videoContent.length - 1; i >= 0; i--) {
      if (this.videoContent[i].keyVideo == keyVideo) {
        this.videoContent.splice(i, 1);
      }
    }
  }

  //con esta función sumamos un día cada vez que se ve un vídeo
  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

}
