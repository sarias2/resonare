import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-explication',
  templateUrl: './modal-explication.component.html',
  styleUrls: ['./modal-explication.component.scss']
})
export class ModalExplicationComponent implements OnInit {
  @Input() p1;
  @Input() p2;
  constructor(public activeModal: NgbActiveModal) { }
  
  ngOnInit() {
  }

  closeExplication(){
    this.activeModal.close();
    sessionStorage.setItem("first_time","1");
  }

  closeModal(){
    this.activeModal.close();
  }

}
