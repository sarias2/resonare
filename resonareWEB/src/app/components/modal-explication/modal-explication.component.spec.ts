import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalExplicationComponent } from './modal-explication.component';

describe('ModalExplicationComponent', () => {
  let component: ModalExplicationComponent;
  let fixture: ComponentFixture<ModalExplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalExplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalExplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
