import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingVideoComponent } from './loading-video.component';

describe('LoadingVideoComponent', () => {
  let component: LoadingVideoComponent;
  let fixture: ComponentFixture<LoadingVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
