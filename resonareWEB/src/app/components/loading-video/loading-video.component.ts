import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loading-video',
  templateUrl: './loading-video.component.html',
  styleUrls: ['./loading-video.component.scss']
})
export class LoadingVideoComponent implements OnInit {

  @Input() p1;
  @Input() type;


  phrase = "Tu video se esta insertando en nuestra base de datos";
  stateAnimation = true;
  logo="../../../assets/images/RESONARE horizontal.png";
  text = "Con tu video ayudas al crecimiento de nuestro red y así mismo ayudas a que la gente conozca tu punto de vista"


  constructor(public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private router: Router) { 

      
    }

  ngOnInit() {
    //con el parametro type se sabe si proviene de la inserción de un vídeo o una hisstoria
    // así mismo se sabe que información mostrar.
    if (this.type=="video") {
      var progress = document.getElementById("cambialeElNombre");
      progress.addEventListener("animationend", () => {
        this.p1 = "Tu vídeo se ha insertado exitosamente";
        this.logo = "../../../assets/icons/checkCompleted.svg";
        document.getElementById('progressBar').style.display = "none";
        document.getElementById('loadingRegisterContent').style.height = "530px";
        this.stateAnimation = false;
      }, false);
      
    } else if (this.type == "historia"){
      var progress = document.getElementById("cambialeElNombre");
      progress.addEventListener("animationend", () => {
        this.p1 = "Tu historia se ha insertado exitosamente";
        this.logo = "../../../assets/icons/checkCompleted.svg";
        document.getElementById('progressBar').style.display = "none";
        document.getElementById('loadingRegisterContent').style.height = "530px";
        this.stateAnimation = false;
      }, false);

    }
  }

  backToHome(){
    //según el parametro type se comporta de manera diferente al momento de cerrar el modal de carga
    switch (this.type) {
      case "video":
        this.activeModal.close();
        this.router.navigate(['/inicio']);
        break;
      case "historia":
        this.activeModal.close();
       
        break;
    
      
    }
   
  }

}
