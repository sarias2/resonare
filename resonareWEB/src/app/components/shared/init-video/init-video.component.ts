import { InitModalExplicationComponent } from './../../../init-modal-explication/init-modal-explication.component';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-init-video',
  templateUrl: './init-video.component.html',
  styleUrls: ['./init-video.component.scss']
})
export class InitVideoComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal,
    private modalService: NgbModal,) { }

  ngOnInit() {
    // var firstTimeInit = sessionStorage.getItem("first_time");
    // if(!firstTimeInit) {
    //   // first time loaded!
    //   sessionStorage.setItem("first_time","1");
    // }
  }

  //cierra el modal
  closeModal() {
    this.activeModal.close();
    setTimeout(() => {
      
      this.openInitExplication();
    }, 500);
  }

  openInitExplication(){
    const modalHistoriesRef = this.modalService.open(InitModalExplicationComponent, { size: 'lg', windowClass: 'initModalExplication' });

  }

}
