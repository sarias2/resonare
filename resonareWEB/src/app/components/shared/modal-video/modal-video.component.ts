import { CreateVideoComponent } from './../../create-video/create-video.component';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DbServiceService } from 'src/app/_services/db-service.service';
import { toTypeScript } from '@angular/compiler';
import { formatDate } from '@angular/common';
import { ModalExplicationComponent } from '../../modal-explication/modal-explication.component';
import { Route, Router } from '@angular/router';
import { MemorialesComponent } from '../../memoriales/memoriales.component';



@Component({
  selector: 'app-modal-video',
  templateUrl: './modal-video.component.html',
  styleUrls: ['./modal-video.component.scss']
})
export class ModalVideoComponent implements OnInit {
  @ViewChild('myTextArea') myTextArea: ElementRef;
  
  @Input() videoContent;
  @Input() videoType;

  video:Object;

  //pra cambiar el color del icono de vistas
  state=true;
  eyes = "../../../../assets/icons/eye.svg"
  eyesW = "../../../../assets/icons/eyeW.svg"


  //vistas del video
  views:number;

  category="#Resonare,#Perdón,#Desplazamiento,#Amor ";
  tweetsdata=[];

  //variables necesarias para actualizar la fecha por cada visita
  dToString:string;
  dateToday = '';
  today= new Date();

  //variables para activar las clases en css
  titleForgotten ="";
  bgForgotten ="";
  textForgotten ="";
  titleContentForgotten ="";

  

  //para el insertar comentario
  commentForm: FormGroup;

  myCommentObject:any;

  //formato para el comentario
  comment: object = {
    body: ""
  }

  getTweets=[
    { body:"Lorem Ipsum es simplemente el texto de relleno de las imprentas"},
    { body:"Lorem Ipsum es simplemente el texto de relleno de las imprentas"},
    { body:"Lorem Ipsum es simplemente el texto de relleno de las imprentas"},
    { body:"Lorem Ipsum es simplemente el texto de relleno de las imprentas"},
    { body:"Lorem Ipsum es simplemente el texto de relleno de las imprentas"},
    { body:"Lorem Ipsum es simplemente el texto de relleno de las imprentas"}
  ];
  constructor(public activeModal: NgbActiveModal, 
    private fb: FormBuilder,
    private dbs : DbServiceService,
    private modalService: NgbModal,
    private router: Router,) { 
    this.commentForm= this.fb.group({
      body: ['']
    });

   
  }

  ngOnInit() {
    //video
    this.videoContent.forEach(element => {
      this.video = element;
    });

    console.log("el vídeo: ", this.video);
    
    // this.category ="#"+this.video["category"];
    // console.log("la categoría para twitter: ", this.category);

    //para que comience arriba
    window.scrollTo(0, 0);
  
    
    //cambio de estilos en el modal, dependiendo si es de tipo normal o olvido
    if (this.videoType=='olvido') {
      //cargar la explicación de como se crea contenido
      setTimeout(() => this.openExplication(), 500);
      this.state=false;
      this.titleForgotten = "titleForgotten";
      this.bgForgotten = "bgForgotten";
      this.textForgotten = "textForgotten";
      this.titleContentForgotten = "titleContentForgotten";
    }
    //haciendo la llamada a twitter
    this.dbs.makeCall().subscribe(
      result=>{
        // console.log("la respuesta de twitter: ", result);
      },
      error => {
        console.log(<any>error);

      });
      //pedir los tweets más frecuentes
      this.dbs.searchcall(this.category).subscribe(
        result =>{
          let test = JSON.stringify(result);
          // console.log("el resultado que JSON: ", test);
          let json = JSON.parse(test);
          this.tweetsdata = json.data.statuses;
          // console.log("this.tweetsdata", this.tweetsdata);
        });
  //   //video
  //  this.videoContent.forEach(element => {
  //   this.video=element;
  //  });
   //función que trae los comentarios
    this.retrieveComments();
    //sumarle un dia a la fecha de fin por cada vista
    this.dateSum(this.videoType);
    //función para actualizar las vistas
    this.updateViews(this.videoType);
  }


  //sumar día por visita
  async dateSum(videoType:any){
    //obtengo la fecha final del vídeo
    let ds = this.video["endDate"];
    //la paso a formato fecha
    let stringToDate = new Date(ds);
    //le sumo un día por medio de la función sumar días
    let sumd = this.sumarDias(stringToDate, +1);
    // console.log("le sumó un día: ", sumd);
    //la vuelvo string para poderla insertar en la base de datos.
    this.dToString = sumd.toString();
    // console.log("la fecha en string: ", typeof(this.dToString));

    this.dateToday = formatDate(sumd, 'dd-MM-yyyy / hh:mm:ss - a', 'en-US', '+PDT');
    switch (videoType) {
      case 'normal':
         await this.dbs.updateNode('videos/' + this.video["keyVideo"], { endDate: this.dToString});
        break;
      case 'olvido':
         await this.dbs.updateNode('forgotten/' + this.video["keyVideo"], { endDate: this.dToString});
        break;
    }
  }

  //con esta función sumamos un día cada vez que se ve un vídeo
  sumarDias(fecha, dias) {
    
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  //función que actualiza el número de vistas del video
  async updateViews(videoType:any){
    console.log("vistas del video: ",this.video["views"]);
    
    this.views = this.video["views"];
    console.log("this.views: ",this.video["views"]);

    this.views +=1;
    //le sumo 1 a la vistas del video
    switch (videoType) {
      case 'normal':
        await this.dbs.updateNode('videos/' + this.video["keyVideo"], { views: this.views});
        break;
      case 'olvido':
        await this.dbs.updateNode('forgotten/' + this.video["keyVideo"], { views: this.views});
        break;
    }
    
  }

  //traer comentarior
  async retrieveComments() {
    const snapshot = await this.dbs.getComments(this.video["keyVideo"], "videos");
    //console.log("los comentarios valor: ", snapshot.val());
    this.myCommentObject = snapshot.val();
    // console.log("myCommentObject: ", this.myCommentObject);
    
    //console.log("tamaño", _.size(snapshot.val()));

  }

  //insertar comentarios
  async sendComment() {

    //obtener el valor del textarea del comentario
    // console.log("cuerpo del comentario: ", this.commentForm.controls.body.value);
    this.comment["body"] = this.commentForm.controls.body.value;

    //limpiar el textarea una vez le da en enviar comentario
    this.myTextArea.nativeElement.value="";
    // console.log("this.video[keyVideo]", this.video["keyVideo"]);

    //insertar comentario
    this.dbs.addComent(this.comment, this.video["keyVideo"]);
  }


  //activa el modal de creación de vídeo
  openToCreateVideo(){
    const modalRef = this.modalService.open(CreateVideoComponent, { size: 'lg', windowClass: 'createVideo' });
    this.activeModal.close();
  }

  //función que abre la explicación
  openExplication() {
    const modalRef = this.modalService.open(ModalExplicationComponent, { size: 'lg', windowClass: 'explicationModal', centered: true });
    modalRef.componentInstance.p1 = "La dinámica de creación de contenido consiste en que el usuario grabe un vídeo en el cual cuente alguna experienca que haya tenido directa o indirectamente con el conflicto armado en Colombia, con el fin de dar a conocer tu experiencia y aportar a la generación de contenido que nutra esta red.";
    modalRef.componentInstance.p2 = "Al contenido creado podrás darle un título y una descripción según tu gusto y podrás escoger una de las categorías suministradas y seleccionar la que creas que encaja más con tu vivencia.";
  }

  //funció que redirigue al apartado de historias
  goToHistories(){
    // this.router.navigate(["/tus-historias"]);
    const modalHistoriesRef = this.modalService.open(MemorialesComponent, { size: 'lg', windowClass: 'MemoriesModal' })
    this.activeModal.close();
  }

  closeModal(){
    this.activeModal.close();
  }

  


}
