import { MemorialesComponent } from './../memoriales/memoriales.component';
import { InitVideoComponent } from './../shared/init-video/init-video.component';
import { MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { ModalVideoComponent } from '../shared/modal-video/modal-video.component';

import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DbServiceService } from 'src/app/_services/db-service.service';
import { log } from 'util';
import { ActivatedRoute } from '@angular/router';


//referencias a p5 y arbol.js
declare var p5: any;
declare const TreeContainer: any;
declare const forgottenTreeContainer: any;


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  //variable que muestra si es normal o del olvido
  option:any;

  //imagen con el ojo
  imgopt=false;

  
//  olvido = "olvido";
//  normal= "normal";

  //contenedor de los datos traidos de firebase
  JsonVideoContainer = [];

  //contenedor para los vídeos que esten en el olvido
  forgottenJsonVideoContainer=[];

  //fecha actual
  today= new Date();


  //contador
  count:number=0;
  countKey=[];

  memoriasVisibles:boolean = false;

  constructor(public dialog:MatDialog,
    private modalService: NgbModal,
    private dbServices: DbServiceService,
    private route: ActivatedRoute) { 

    }

  async ngOnInit() {

    //para que comience arriba
    window.scrollTo(0, 0);
   

    //abrir modal video inicial
    // var firstTimeInit = sessionStorage.getItem("first_time");
    // if(!firstTimeInit) {
      // first time loaded!
      setTimeout(() => this.openDialog());
      // sessionStorage.setItem("first_time","1");
    // }
    


    //traigo los vídeos del nodo videos
    this.getVideos();

    //traigo los vídeos del nodo forgotten
    
    this.getvideosForoggten();

    // console.log("los olvidados: ", this.forgottenJsonVideoContainer);
    // console.log("videos de firebase con fecha vigente: ", this.JsonVideoContainer);

    //doy clic para mandar la información al p5
    document.getElementById('initButtonClick').click();
    // console.log("initButtonClick, clic!");
    document.getElementById('forgottenButtonClick').click();
    // console.log("forgottenButtonClick, clic!");
  }

  //función que trae los videos apenas carga la página
  async getVideos(){
    const resultVideos = await this.dbServices.getVideos('videos');
    Object.keys(resultVideos.val()).forEach(async element => {
      // console.log("resultVideos.val()", resultVideos.val());
      //si la fecha final del video es mayor a la actual, se inserta como un vídeo normal
      let endDate = new Date(resultVideos.val()[element]["endDate"]);
      if (endDate.getTime() > this.today.getTime()) {
        // console.log("entró al if");
        this.JsonVideoContainer.push({
          keyVideo: element,
          category: resultVideos.val()[element]["category"],
          date: resultVideos.val()[element]["date"],
          endDate: resultVideos.val()[element]["endDate"],
          title: resultVideos.val()[element]["title"],
          description: resultVideos.val()[element]["description"],
          video: resultVideos.val()[element]["video"],
          views: resultVideos.val()[element]["views"],
          videoSuplente: resultVideos.val()[element]["videoSuplente"],
        })
      } else {
        let videoToDelete = {
          keyVideo: element,
          category: resultVideos.val()[element]["category"],
          date: resultVideos.val()[element]["date"],
          endDate: resultVideos.val()[element]["endDate"],
          title: resultVideos.val()[element]["title"],
          description: resultVideos.val()[element]["description"],
          video: resultVideos.val()[element]["video"],
          videoSuplente: resultVideos.val()[element]["videoSuplente"],
          views: 0
        }
        //se elimina el video del nodo de videos
        await this.dbServices.removeNode('videos/' + videoToDelete["keyVideo"]);
        // se inserta el video en el nodo de olvidados
        const ff = await this.dbServices.addvideosForgotten(videoToDelete);

        // console.log("el else");
      }
    });
  }
//función que trae los videos de forgotten apenas carga la página
  async getvideosForoggten(){
    //traigo los vídeos de olvidados
    const resultVideosForgotten = await this.dbServices.getVideos('forgotten');
    console.log("el tipo: ",resultVideosForgotten.val());
    if (resultVideosForgotten.val()==null) {
        console.log("esta null, no hacer nada");
    }else{
      Object.keys(resultVideosForgotten.val()).forEach(async element => {
        // console.log("entró al if");
        // console.log("resultVideosForgotten.val()", resultVideosForgotten.val());
        //si la fecha final del video es mayor a la actual, se inserta como un vídeo normal
  
        //verifico que el numero de vistas sea mayor a 5 para saber si lo debe de eliminar de olvidados y
        //volver a poner en el nodo de videos normales.
        if (resultVideosForgotten.val()[element]["views"] > 5) {
          //reestablecemos la fecha de final como la fecha actual más 5 días
          let endDate = new Date();
          //se asigna la fecha de fin en formato date
          var endDateCobtent = this.sumarDias(endDate, +5);
          //pasando la fecha de creación  string
          let newEndDate = endDateCobtent.toDateString();
  
          let video = {
            keyVideo: element,
            category: resultVideosForgotten.val()[element]["category"],
            date: resultVideosForgotten.val()[element]["date"],
            endDate: newEndDate,
            title: resultVideosForgotten.val()[element]["title"],
            description: resultVideosForgotten.val()[element]["description"],
            video: resultVideosForgotten.val()[element]["video"],
            views: resultVideosForgotten.val()[element]["views"],
            videoSuplente: resultVideosForgotten.val()[element]["videoSuplente"]
          }
          //elimino el vídeo del nodo forgotten
          this.removeForgotten(resultVideosForgotten.val()[element]["views"], element, video);
  
          //inserto el vídeo en el nodo de video
          const ff = await this.dbServices.addvideos(video);
  
          //actualizo la key del video con la de la nueva insersción
          await this.dbServices.updateNode('videos/' + ff, { keyVideo: ff });
  
        } else {
          //cuando las vistas del video son menor a 5
          this.forgottenJsonVideoContainer.push({
            keyVideo: element,
            category: resultVideosForgotten.val()[element]["category"],
            date: resultVideosForgotten.val()[element]["date"],
            endDate: resultVideosForgotten.val()[element]["endDate"],
            title: resultVideosForgotten.val()[element]["title"],
            description: resultVideosForgotten.val()[element]["description"],
            video: resultVideosForgotten.val()[element]["video"],
            views: resultVideosForgotten.val()[element]["views"],
            videoSuplente: resultVideosForgotten.val()[element]["videoSuplente"]
          })
        }
      });
    }
  }

  //función para abrir el modal video de explicación
  openDialog(){
    // var firstTime = sessionStorage.getItem("first_time");
    // if (firstTime != "1") {
      
      const modalRef = this.modalService.open(InitVideoComponent, { size: 'lg', windowClass: 'initVideo' });
    // }

  }

  //función que abre el modal de las historias
  openHistories(){
    const modalHistoriesRef = this.modalService.open(MemorialesComponent, { size: 'lg', windowClass: 'MemoriesModal' })
  }

  //función para mandar la data a p5
  sendDataToP5() {

    //click
    // console.log("dio click al arbol normal: ");
    //instancia  de tipo treeContainer.
    var treeContainer = new TreeContainer(this.getTreeData, this, (this.JsonVideoContainer),'normal');
    //instacia de p5 para asignar a un elemento html el arbol.
    new p5(treeContainer, window.document.getElementById('container'));

  }
  //función para mandar la data al arbol del olvido
  sendDataToP5F() {
    
    //click
    // console.log("dio click al  arbol del olvido: ");
    //instancia  de tipo treeContainer.
    var forgottentreeContainer = new forgottenTreeContainer(this.getTreeData, this, (this.forgottenJsonVideoContainer),'olvido');
    //instacia de p5 para asignar a un elemento html el arbol.
    new p5(forgottentreeContainer, window.document.getElementById('containerOlvido'));
  }

  //Función que retorna el objeto con los videos desde p5 y el scope
  public getTreeData(video: any, scope: any,type:any) {
    
    // console.log("video en angular", video);
      console.log("llegue");
    if (video === null) {
      // console.log("esta null papá");
      
    }else{
      scope.option =type;
      // console.log("video: ", video)
      //probando si puedo sumar el id cada vez que se presiona al session storage
      scope.count +=1;
      for (const v in video) {
        if (video.hasOwnProperty(v)) {
          const element = video[v];
          scope.countKey.push({ 
            keyVideo: element.keyVideo,
            category: element.category,
            date: element.date,
            endDate: element.endDate,
            title: element.title,
            description: element.description,
            views: element.views,
            video: element.video,
            videoSuplente: element.videoSuplente,

          });
          // sessionStorage.setItem("key" + scope.count, element.keyVideo);
        }
      }

      
      // console.log("el arreglo de keys vistas: ", scope.countKey);

      scope.dbServices.saveProvisionally(scope.countKey);
      scope.openModalContent(video, scope.option );

      // console.log("Session Storage: ", sessionStorage);
      // console.log("Session Storage.length: ", sessionStorage.length);
      

    }   
  }

  openModalContent (video:any, type:any){
    // console.log("el tipo es: ", type);
  //mandamos la información al modal
  const sendDataModal = this.modalService.open(ModalVideoComponent, { size: 'lg', windowClass: 'ModalVideo' });
  sendDataModal.componentInstance.videoContent = video;
  sendDataModal.componentInstance.videoType = type;
  }

  async removeForgotten(views: any, videoKey:any, video:any) {
    
      // console.log("tiene más de 5 vistas el video: ", views);
      //eliminando el vídeo de forgotten
      await this.dbServices.removeNode('forgotten/' + videoKey);
  }

  //con esta función sumamos un día cada vez que se ve un vídeo
  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  //mostrar y esconder la sección de historiales
  showTestimonials(op){
    switch (op) {
      case 'show':
        this.imgopt=true;
        break;
      case 'hide':
        this.imgopt = false;
        break;
    
     
    }
  }


}
