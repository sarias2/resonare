import { CreateHistoryComponent } from './../../create-history/create-history.component';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalExplicationComponent } from '../modal-explication/modal-explication.component';
import { DbServiceService } from 'src/app/_services/db-service.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-memoriales',
  templateUrl: './memoriales.component.html',
  styleUrls: ['./memoriales.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MemorialesComponent implements OnInit {

  //hisotiresContent - contendor de las historias
  historiesContent=[];


  //los vídeos que ha visto el usuario en su viaje
  videoContent=[];

  //fecha
  today= new Date();

  //vistas de la historia
  views: number;

  constructor(private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private dbs: DbServiceService,
    private router: Router,
    ) { }

  async ngOnInit() {

    this.videoContent = this.dbs.getProvisionallyVideos();
    //traemos los vídeos que vio el usuario en su viaje por la plataforma
    const resultHistories = await this.dbs.getHistories('histories');
    let videos = [];
    Object.keys(resultHistories.val()).forEach(async element => {
      //fecha final del vídeo convertida a tiempo
      let endDate = new Date(resultHistories.val().endDate);
      //si la fecha de la historia es menor al día actual se eliminará
      if (endDate.getTime() < this.today.getTime()) {
        let historyToDelete = {
          keyHistory: element,
          date: resultHistories.val()[element]["date"],
          endDate: resultHistories.val()[element]["endDate"],
          title: resultHistories.val()[element]["title"],
          videos: resultHistories.val()[element]["videos"],
          views: 0
        }
        await this.dbs.removeNode('histories/' + historyToDelete["keyHistory"]);
      }else{
        console.log("todas las fechas aún tienen tiempo de vida!");
        console.log("---------------------------------------------");
          this.historiesContent.push({
            keyHistory: element,
            videos: resultHistories.val()[element]["videos"],
            date: resultHistories.val()[element]["date"],
            endDate: resultHistories.val()[element]["endDate"],
            title: resultHistories.val()[element]["title"],
            views: resultHistories.val()[element]["views"],
          })
      }
    });
   

    console.log("las historias: ", this.historiesContent);

    //cargar la explicación de como se crea tu historia
    setTimeout(() => this.openExplication(), 500);
  }

  //función que abre la explicación
  openExplication() {

    var firstTime = sessionStorage.getItem("first_time_memoriales");
    if(firstTime != "1") {
      // first time loaded!
      sessionStorage.setItem("first_time_memoriales","1");
      
      const modalRef = this.modalService.open(ModalExplicationComponent, { size: 'lg', windowClass: 'explicationModal', centered: true });
      //explicación ajustada SAL
      modalRef.componentInstance.p1 = "En las historias puedes compartir tu experiencia en la plataforma, cada historia representa tu recorrido por los videos que te llamaron la atención y quieres también que otros vean.";
      modalRef.componentInstance.p2 = "Para guardar tu historia y hacerla parte de nuestro proyecto, solo basta con darle un nombre.";
    }
  }


  //redireccionar al home
  goToHome(){
    // let params: NavigationExtras = {
    //   queryParams: {
    //     "type": "inicio"
    //   }
    // };
    // this.router.navigate(["/inicio"], params);
  }

  //abre el modal de creación de historia
  openModalCH(){
    const modalRef = this.modalService.open(CreateHistoryComponent, { size: 'lg', windowClass: 'createHistoryModal', centered: true});
  }

  //función que actualiza el número de vistas de la historia
  async updateViews(keyHistory: any) {
    // //se asigna la fecha de fin en formato date
    // var endDateCobtent = this.sumarDias(this.today, +1);

    // //pasando la fecha de creación  string
    // let endDateString = endDateCobtent.toDateString();
    for (const h in this.historiesContent) {
      if (this.historiesContent.hasOwnProperty(h)) {
        const element = this.historiesContent[h];
        if (element.keyHistory == keyHistory) {
          //sumar una vista a la historia
          element.views = element.views +=1;
          //sumar 1 día más a la historia que le hacen clic
          let sumd = this.sumarDias(new Date(element.endDate),1);
          let newEndDate:string = sumd.toString();
          element.endDate = newEndDate;


          await this.dbs.updateNode('histories/' + keyHistory, { views: element.views, endDate: newEndDate });
        }
      }
    }
    
    console.log("el id de la historia: ", keyHistory);
  }

  //con esta función sumamos un día cada vez que se ve un vídeo
  sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  //cierra el modal
  closeModal(){
    this.activeModal.close();
  }



}
