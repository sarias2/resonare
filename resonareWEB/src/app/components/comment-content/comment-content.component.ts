import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-comment-content',
  templateUrl: './comment-content.component.html',
  styleUrls: ['./comment-content.component.scss']
})
export class CommentContentComponent implements OnInit {

  @Input() comment: any;
  filterComments: Array<string>;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    this.getBodyFromComment(changes.comment.currentValue);
  }

  ngOnInit() {
    //console.log("lo que me llegó papá: ", "chao papa", this.comment);
    this.getBodyFromComment(this.comment);
    //console.log('el cuerpo', this.filterComments);
  }

  getBodyFromComment(comments: any) {
    this.filterComments = Object.keys(comments).map(k => { return comments[k] });
    //console.log("filtersComments: ",this.filterComments);

  }

}
