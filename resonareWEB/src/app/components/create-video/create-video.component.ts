import { LoadingVideoComponent } from './../loading-video/loading-video.component';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ModalExplicationComponent } from './../modal-explication/modal-explication.component';
import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { formatDate } from '@angular/common';

//firebase
import * as firebase from "firebase/app";



//declarando el elemento videoRTC
import * as RecordRTC from 'recordrtc';
import { DbServiceService } from 'src/app/_services/db-service.service';

@Component({
  selector: 'app-create-video',
  templateUrl: './create-video.component.html',
  styleUrls: ['./create-video.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateVideoComponent implements OnInit {

  //variables para la fecha actual
  today = new Date();
  endDate="";
  dateToday = '';

  //ngmodel de categorías
  category:string;

  //vistas
  views:number=0;

  //contenedor de blob resultante del vídeo
  

  //formumarlio
  videoForm: FormGroup;

  //inicializamos el nuevo archivo de tipo file
  file:any;

  //haciendo referencia al hijo
  @ViewChild('video') video: any;
  private stream: MediaStream;
  private recordRTC: any;
  

  constructor(
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private dbs: DbServiceService) {

    //fecha actual
    this.dateToday = formatDate(this.today, 'dd-MM-yyyy / hh:mm:ss - a', 'en-US', '+PDT');
    //creamos el formato para el formulario, así mismo las validaciones para cada campo
    this.videoForm = this.fb.group({
      title: [''],
      // endDate: [''],
      description: [''],
      category: new FormControl()
    });

   }

  // convenience getter for easy access to form fields
  get f() { return this.videoForm.controls; }

  ngAfterViewInit() {
    // Poniendo el estado inicial del elemento video
    let video: HTMLVideoElement = this.video.nativeElement;
    video.muted = false;
    video.controls = true;
    video.autoplay = false;
  }

  ngOnInit() {
    //cargar la explicación de como se crea contenido
    setTimeout(() => this.openExplication(), 1000);

  }

  //con esta función sumamos un día cada vez que se ve un vídeo
   sumarDias(fecha, dias) {
  fecha.setDate(fecha.getDate() + dias);
  return fecha;
}

  async insertVideo(){
    //se le asigna el nombre al vídeo
    // this.recordRTC.save(this.videoForm.value.title+'.webm');
    
    //pasando la fecha de creación  string
    let todayString = this.today.toDateString();

    //se asigna la fecha de fin en formato date
   var endDateCobtent= this.sumarDias(this.today,+5);

    //pasando la fecha de creación  string
    let endDateString = endDateCobtent.toDateString();


   
   //se le asigna el formato a mostrar en el vídeo del modal
  //  this.endDate = formatDate(endDateCobtent, 'dd-MM-yyyy / hh:mm:ss - a', 'en-US', '+PDT');
  //  console.log("la fecha de fin: ", endDateCobtent);
  
    
    
    //cogemos los datos almacenados en los campos del formulario
    let videoData={
      "title": this.videoForm.value.title,
      "date": this.dateToday,
      "endDate": endDateString,
      "description": this.videoForm.value.description,
      "category": this.videoForm.value.category,
      'video': "no hay",
      "views": this.views,
      'videoSuplente':"acá va la url de firebase"
      
    }
    // console.log("videoData: ", videoData);

    // inserto el video en firebase SIN EL ENALCE A FIREBASE del vídeo
    const result = await this.dbs.addvideos(videoData);

    // const data = await this.dbs.pushUpload(test);
    // console.log("data PRUEBA: ", data);
    //inserto EL VIDEO EN EL STORAGE
    let document = await firebase.storage().ref('uploads/' + this.file.name).put(this.file);
    let url = await document.ref.getDownloadURL();

    //actualizo el nodo con LA URL DEL VÍDEO YA EN EL STORAGE DE FIREBASE
    await this.dbs.updateNode('videos/' + result, { videoSuplente: url });

    // console.log("url: ", url);

    //abrir modal de retroalimentación
    const modalRefLogin = this.modalService.open(LoadingVideoComponent, { size: 'lg', windowClass: 'loadingVideoModal' });
    modalRefLogin.componentInstance.p1 = "Tu video se esta insertando en nuestra base de datos";
    modalRefLogin.componentInstance.type = "video";
    this.activeModal.close(); 

    //reseteamos los valores del forumario
    this.videoForm.value.title ="";
    this.videoForm.value.description ="";
    this.videoForm.value.category ="";
    
    
    
  }
  //función para comenzar a grabar
  startRecording() {
    // console.log("entró");
    
    let mediaConstraints = {
      audio: true,
      video: { width: 720, height: 399.5 }
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }

  //función para detener el video
  stopRecording() {
    let recordRTC = this.recordRTC;
    recordRTC.stopRecording(this.processVideo.bind(this));
    let stream = this.stream;
    stream.getAudioTracks().forEach(track => track.stop());
    stream.getVideoTracks().forEach(track => track.stop());
  }

  //procesando el vídeo
  processVideo(audioVideoWebMURL) {
    let video: HTMLVideoElement = this.video.nativeElement;
    let recordRTC = this.recordRTC;
    video.src = audioVideoWebMURL;
    this.toggleControls();
    //obtengo el blob luego de que paran la grabación del vídeo
    var recordedBlob = recordRTC.getBlob();
    //-------------------------------------------
    console.log("el blob: ", typeof(recordedBlob));

    //VUELVO UN ARCHIVO DE TIPO 'FILE' EL VIDEO QUE GRABARON 
    //LE PONGO EL NOMBRE QUE EL USUARIO LE ASIGNE
    this.file = new File([recordedBlob], this.videoForm.value.title+'.webm',{
      type:'video/webm'
    });

    // console.log("file: ", this.file);
    // var formData = new FormData();
    // formData.append('file', file);
    // console.log("formData: ", formData);
    //-------------------------------------------
    
    recordRTC.getDataURL(function (dataURL) { 
      // console.log("la url? : ", typeof(dataURL) );
    });
  }

  //función en la cual devuelve el llamado
  successCallback(stream: MediaStream) {
    var options = {
      mimeType: 'video/webm', // or video/webm\;codecs=h264 or video/webm\;codecs=vp9
      audioBitsPerSecond: 128000,
      videoBitsPerSecond: 128000,
      bitsPerSecond: 128000, // if this line is provided, skip above two
      frameInterval: 90,
      sampleRate: 96000,

      // used by StereoAudioRecorder
      // the range 22050 to 96000.
      // let us force 16khz recording:
      desiredSampRate: 16000,

      // used by StereoAudioRecorder
      // Legal values are (256, 512, 1024, 2048, 4096, 8192, 16384).
      bufferSize: 16384,

      // used by StereoAudioRecorder
      // 1 or 2
      numberOfAudioChannels: 2,

      // used by WebAssemblyRecorder
      frameRate: 30,

      // used by WebAssemblyRecorder
      bitrate: 128000
    };
    this.stream = stream;
    this.recordRTC = RecordRTC(stream, options);
    this.recordRTC.startRecording();
    let video: HTMLVideoElement = this.video.nativeElement;
    // video.src = window.URL.createObjectURL(stream);
    this.toggleControls();
  }
  errorCallback() {
    //handle error here
    // console.log("entró al error");
    
  }
  //controles para evitar el eco una vez el vídeo se este grabando.
  toggleControls() {
    let video: HTMLVideoElement = this.video.nativeElement;
    video.muted = !video.muted;
    video.controls = !video.controls;
    video.autoplay = !video.autoplay;
  }

  //función que abre la explicación
  openExplication(){
    const modalRef = this.modalService.open(ModalExplicationComponent, { size: 'lg', windowClass: 'explicationModal', centered: true  });
    modalRef.componentInstance.p1 = "La dinámica de creación de contenido consiste en que el usuario grabe un vídeo en el cual cuente alguna experienca que haya tenido directa o indirectamente con el conflicto armado en Colombia, con el fin de dar a conocer tu experiencia y aportar a la generación de contenido que nutra esta red.";
    modalRef.componentInstance.p2 = "Al contenido creado podrás darle un título y una descripción según tu gusto y podrás escoger una de las categorías suministradas y seleccionar la que creas que encaja más con tu vivencia.";
  
  
    
  }
  closeModal(){
    this.activeModal.close();   
  }

}
