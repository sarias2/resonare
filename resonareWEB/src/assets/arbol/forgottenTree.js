var forgottenTreeContainer = function (treeParameter, scope, JsonContainer,type) {
  // console.log("estoy en el js: ", scope, JsonContainer);
  return function (sketch) {
    var branchArray = [];
    var container = [];
    var containerObj = Object;
    // console.log("afuera:", container);
    sketch.setup = function () {
      sketch.createCanvas(625, 500);
      // sketch.background(255,100);
      // sketch.stroke(255);
      sketch.noStroke();
      sketch.scale(1,-1);



      //First branch
      var a = sketch.createVector(sketch.width / 2, sketch.height);
      var b = sketch.createVector(sketch.width / 2, sketch.height - 100);

      var root = new Branch(a, b, 180, '#CC6600');
      // branchArray[0] = root;
      let countPar = 2;
      let countImpar = 2;
      for (let index = 0; index < JsonContainer.length; index++) {
        let count = 0;
        count += index;

        if (index == 0) {
          branchArray[index] = new Branch(a, b, 180, '#777675');
          // branchArray[index] = new Branch(a, b, 180, '#CC6600');
        } else if (index % 2 == 1 && index == 1) {
          //     // let count = 0;
          // console.log("impar y solo para 1 - count vale: ",count);
          branchArray[index] = branchArray[count - 1].branchRight('#777675');
          // branchArray[index] = branchArray[count - 1].branchRight('#0066CC');

        } else if (index % 2 == 1) {

          // console.log("impar mayor que 1 y count vale: ", count);
          // console.log("Countimpar sin sumar ", countImpar);
          branchArray[index] = branchArray[index - countImpar].branchRight('#777675');
          // branchArray[index] = branchArray[index - countImpar].branchRight('#0066CC');
          countImpar += 1;
          // console.log("Countimpar después de sumar ", countImpar);
        }
        if (index % 2 == 0 && index != 0) {
          // console.log("par ",index);
          // console.log("countPar antes de sumar: ", countPar);
          branchArray[index] = branchArray[count - countPar].branchLeft('#777675');
          // branchArray[index] = branchArray[count - countPar].branchLeft('#00CC00');
          countPar += 1;
          // console.log("countPar después de sumanr: ", countPar);
        }
      }
    }

    sketch.mousePressed = function (params) {
      for (var i = 0; i < branchArray.length; i++) {
        if (branchArray[i].clickedBranch(sketch.mouseX, sketch.mouseY)) {
          // console.log("posicionMouse: (" + sketch.mouseX + "," + sketch.mouseY+")");
          // console.log("esta en la posición: ",i);
          // console.log("posición del vídeo: ",JsonContainer[i]);
          container.push(
            JsonContainer[i]["title"],
            JsonContainer[i]["category"],
            JsonContainer[i]["date"],
            JsonContainer[i]["endDate"],
            JsonContainer[i]["description"],
            JsonContainer[i]["keyVideo"],
            JsonContainer[i]["video"],
            JsonContainer[i]["views"],
            JsonContainer[i]["videoSuplente"],
          );

          console.log("hizo clic en la rama");
          console.log("container en p5: ", container);
          // console.log("JsonContainer[i][videoId]", JsonContainer[i]["videoId"]);

          //do when clicked branch
        } else {

          // container = [];
          // console.log("Container vacio: ", container);
          // console.log("container length: ", container.length);
        }
      }




      if (container === undefined || container.length === 0) {
        containerObj = null;
        console.log("entro al if");
      } else {
        console.log("entro al else");
        containerObj = [{
          "title": container[0],
          "category": container[1],
          "date": container[2],
          "endDate": container[3],
          "description": container[4],
          "keyVideo": container[5],
          "video": container[6],
          "views": container[7],
          "videoSuplente": container[8]
        }];

      }
      // console.log("containerObj en p5: ", containerObj);
      // containerObj = [{
      //   "videoTitle": container[0],
      //   "categoryId": container[1],
      //   "channelId": container[2],
      //   "videoId": container[3],
      //   "description": container[4],
      //   "view": container[5],
      //   "dateTime": container[6],
      //   "endDate": container[7]
      // }];

      treeParameter(containerObj, scope,type);
      container = [];
      containerObj = [];
      console.log(" CONTAINER despues: ", container);
      console.log(" containerObj despues: ", containerObj);
    }

    sketch.draw = function () {
      for (var i = 0; i < branchArray.length; i++) {
        branchArray[i].show();
      }

    }

    function Branch(begin, end, angle, color, size = 100) {
      this.begin = begin;
      this.end = end;
      this.color = color;
      this.angle = angle;
      this.size = size;




      this.show = function () {
        if (this.color) {
          sketch.stroke(this.color);
        } else {
          sketch.stroke(255);
        }
        sketch.line(this.begin.x, this.begin.y, this.end.x, this.end.y);



      }
      // var rot = sketch.PI / 4;
      this.branchRight = function (color) {
        let angulo = (this.angle - 20);
        let rotation = angulo * (Math.PI / 180);
        // console.log("angle",this.angle);
        var a = sketch.createVector(this.end.x, this.end.y);
        var b = sketch.createVector(a.x + (Math.sin(rotation) * this.size), a.y + (Math.cos(rotation) * this.size));
        sketch.stroke(0); // Setting the outline (stroke) to black
        sketch.fill(150);
        sketch.ellipse(this.end.x, this.end.y, 20);
        return new Branch(a, b, angulo, color, this.size * 0.7);


      }
      this.branchLeft = function (color) {
        let angulo = (this.angle + 45);
        let rotation = angulo * (Math.PI / 180);


        // console.log("angle",this.angle);
        var a = sketch.createVector(this.end.x, this.end.y);
        var b = sketch.createVector(a.x + (Math.sin(rotation) * this.size), a.y + (Math.cos(rotation) * this.size));
        sketch.stroke(0); // Setting the outline (stroke) to black
        sketch.fill(150);
        sketch.ellipse(this.end.x, this.end.y, 20);
        return new Branch(a, b, angulo, color, this.size * 0.7);
      }

      this.clickedBranch = function (x, y) {
        if (x > 0 && y > 0) {
          let x1 = this.begin.x;
          let y1 = this.begin.y;
          let x2 = this.end.x;
          let y2 = this.end.y;

          var A = x - x1;
          var B = y - y1;
          var C = x2 - x1;
          var D = y2 - y1;

          var dot = A * C + B * D;
          var len_sq = C * C + D * D;
          var param = -1;
          if (len_sq != 0) { //in case of 0 length line
            param = dot / len_sq;
          }
          var xx, yy;

          if (param < 0) {
            xx = x1;
            yy = y1;
          } else if (param > 1) {
            xx = x2;
            yy = y2;
          } else {
            xx = x1 + param * C;
            yy = y1 + param * D;
          }

          var dx = x - xx;
          var dy = y - yy;
          let distance = Math.sqrt(dx * dx + dy * dy);

          if (distance < 5) {
            // console.log("this.begin: (" + this.begin.x + "," + this.begin.y+")");
            // console.log("this.end: (" + this.end.x + "," + this.end.y + ")");
            // console.log("mouse: ("+x+","+y+")");
            // console.log("Distancia: ", distance);
            // console.log("_________________________");
            this.color = "#aa0000";
            return true;
          } else {
            return false;
          }
        } else {
          return false
        }
      }
    }
  }
};